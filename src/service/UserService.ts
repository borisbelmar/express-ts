import { Request, Response } from 'express'
import { usersMock } from '../__mocks__/User'
import { IUser } from '../interfaces/IUser'

export default class UserService {
  findAll (_req: Request, res: Response): void {
    // Get mock users
    const users: IUser[] = usersMock
    res.send(users)
  }
}
