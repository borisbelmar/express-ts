import { IUser } from '../interfaces/IUser'

export const usersMock:IUser[] = [
  { id: 1, name: 'Luis', age: 24 },
  { id: 2, name: 'Pedro', age: 18, description: 'Una descripción' },
  { id: 3, name: 'María', age: 21 }
]
export const userMock:IUser = usersMock[0]
export const findMockUserById = (id: number): IUser => usersMock.filter(user => user.id === id)[0]
