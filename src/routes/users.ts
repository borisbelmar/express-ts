import { Router } from 'express'
import UserService from '../service/UserService'

const router: Router = Router()
const service: UserService = new UserService()

router.get('/', service.findAll)

export default router
