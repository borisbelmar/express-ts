import { Router, Request, Response } from 'express'
import users from './users'

const router:Router = Router()


router.get('/', (_req: Request, res: Response) => {
  res.status(200).send({ message: 'Express App with Typescript' })
})

router.use('/users', users)

export default router
