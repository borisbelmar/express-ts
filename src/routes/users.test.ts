import supertest from 'supertest'
import app from '../app'
import { usersMock } from '../__mocks__/User'

const request = supertest(app)

describe('Users routes', () => {
  it('Should find all users', async done => {
    const res = await request.get('/users')
    expect(res.status).toBe(200)
    expect(res.body).toEqual(usersMock)
    expect(res.body.length).toBe(3)
    done()
  })
})
