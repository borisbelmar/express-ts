import express, { json, Application } from 'express'
import morgan from 'morgan'

import cors from './middleware/cors'
import routes from './routes'

// Inicialización de express
const app:Application = express()

// Middlewares
app.use(morgan('dev', { skip: () => process.env.NODE_ENV === 'test' }))
app.use(json())
app.use(cors)

// Context Variables
app.set('PORT', process.env.PORT || 4000)

// Routes
app.use('/', routes)

/* istanbul ignore next */
if (process.env.NODE_ENV !== 'test') {
  app.listen(app.get('PORT'), () => {
    console.log('Server on port', app.get('PORT'))
  })
}

export default app
