FROM node:12.18.2-alpine as builder
COPY ["package.json", "yarn.lock", "/app/"]
WORKDIR /app
RUN yarn
COPY [".", "/app/"]
RUN yarn build

FROM node:12.18.2-alpine as test
WORKDIR /app
COPY --from=builder ["/app/", "/app/"]
RUN yarn test

FROM node:12.18.2-alpine
WORKDIR /app
COPY --from=test ["/app/", "/app/"]
EXPOSE 4000
CMD ["yarn", "prod"]